import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchComponent } from './pages/branch/branch.component';
import { HomeComponent } from './pages/home/home.component';
import { ViewAgentsComponent } from './pages/view-agents/view-agents.component';
import { EditBranchComponent } from './pages/edit-branch/edit-branch.component';
import { StaffComponent } from './pages/staff/staff.component';
import { EditStaffComponent } from './pages/edit-staff/edit-staff.component';
import { WidgetsComponent } from './pages/widgets/widgets.component';
import { ReviewsComponent } from './pages/reviews/reviews.component';
import { ReviewPlatformComponent } from './pages/review-platform/review-platform.component';
import { ReviewInvitationComponent } from './pages/review-invitation/review-invitation.component';
import { MissingDataComponent } from './pages/missing-data/missing-data.component';


const routes: Routes = [
  { path: '', redirectTo: '/branch-data', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'view-agents-data', component: ViewAgentsComponent },
  { path: 'branch-data', component: BranchComponent },
  { path: 'edit-branch', component:EditBranchComponent},
  { path: 'staff ', component:StaffComponent},
  { path: 'edit-staff ', component:EditStaffComponent},
  { path: 'widgets ', component:WidgetsComponent},
  { path: 'reviews ', component:ReviewsComponent},
  { path: 'review-platform ', component:ReviewPlatformComponent},
  { path: 'review-invitation ', component:ReviewInvitationComponent},
  { path: 'missing-data', component:MissingDataComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
