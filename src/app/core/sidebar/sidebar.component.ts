import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
}



@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItem: RouteInfo[] = [];
  constructor() {

    this.menuItem =  [ 
    { path: 'home', title:'Home',icon:''},
    { path: 'view-agents-data', title:'View Agents data',icon:''},
    { path: 'branch-data', title:'Branch data',icon:''},
    { path: 'edit-branch', title:'Edit branch',icon:''},
    { path: 'staff ', title:'Staff',icon:''},
    { path: 'edit-staff ', title:'Edit Staff ',icon:''},
    { path: 'widgets ', title:'Widgets ',icon:''},
    { path: 'reviews ', title:'Reviews',icon:''},
    { path: 'review-platform ', title:'Review platform ',icon:''},
    { path: 'review-invitation ', title:'Review invitation ',icon:''},
    { path: 'missing-data', title:'',icon:'Missing data'},
  ]
    
   }

  ngOnInit(): void {
    
  }

}
