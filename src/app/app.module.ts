import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BranchComponent } from './pages/branch/branch.component';
import { HeaderComponent } from './core/header/header.component';
import { SidebarComponent } from './core/sidebar/sidebar.component';
import { HomeComponent } from './pages/home/home.component';
import { ViewAgentsComponent } from './pages/view-agents/view-agents.component';
import { EditBranchComponent } from './pages/edit-branch/edit-branch.component';
import { StaffComponent } from './pages/staff/staff.component';
import { EditStaffComponent } from './pages/edit-staff/edit-staff.component';
import { WidgetsComponent } from './pages/widgets/widgets.component';
import { ReviewsComponent } from './pages/reviews/reviews.component';
import { ReviewPlatformComponent } from './pages/review-platform/review-platform.component';
import { ReviewInvitationComponent } from './pages/review-invitation/review-invitation.component';
import { MissingDataComponent } from './pages/missing-data/missing-data.component';

@NgModule({
  declarations: [
    AppComponent,
    BranchComponent,
    HeaderComponent,
    SidebarComponent,
    HomeComponent,
    ViewAgentsComponent,
    EditBranchComponent,
    StaffComponent,
    EditStaffComponent,
    WidgetsComponent,
    ReviewsComponent,
    ReviewPlatformComponent,
    ReviewInvitationComponent,
    MissingDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
